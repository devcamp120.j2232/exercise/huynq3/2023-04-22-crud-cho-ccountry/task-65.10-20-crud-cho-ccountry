package com.devcamp.countryregioncrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregioncrud.model.CCountry;

public interface CountryRepository extends JpaRepository<CCountry, Long>{
    CCountry findByCountryCode(String countryCode);
}
